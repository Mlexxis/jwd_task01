package by.etc.jwd.task01.creator;

import by.etc.jwd.task01.entity.Point;
import by.etc.jwd.task01.entity.Triangle;
import by.etc.jwd.task01.exception.CreatorException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class TriangleCreatorTest {

    private List<Point> points;
    private Point one;
    private Point two;
    private Point three;
    private TriangleCreator creator;

    @BeforeTest
    private void setUp() {
        creator = new TriangleCreator();
        one = new Point(1.0, 1.0);
        two = new Point(3.0, 3.0);
        three = new Point(5.0, 1.0);
        points = new ArrayList<>();
        points.add(one);
        points.add(two);
        points.add(three);
    }


    @Test (groups = {"creator"})
    public void create_PointList_ShouldCreateProperTriangle() {
        Triangle expected = new Triangle();
        expected.setLeftVertex(one);
        expected.setMiddleVertex(two);
        expected.setRightVertex(three);

        Triangle actual = new Triangle();
        try {
            actual = creator.create(points);
        } catch (CreatorException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(actual, expected);
    }

    @Test (groups = {"creator"})
    public void create_VarArgsCoords_ShouldCreateProperTriangle() {
        Triangle expected = new Triangle();
        expected.setLeftVertex(one);
        expected.setMiddleVertex(two);
        expected.setRightVertex(three);

        Triangle actual = new Triangle();
        try {
            actual = creator.create(1.0, 1.0, 3.0, 3.0, 5.0, 1.0);
        } catch (CreatorException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(actual, expected);
    }

    @Test (groups = {"creator"}, expectedExceptions = {CreatorException.class})
    public void create_VarArgsCoords_ShouldThrowExceptionOnWrongArguments() throws CreatorException {
        creator.create(1.0, 1.0, 3.0);
    }

    @Test (groups = {"creator"}, expectedExceptions = {CreatorException.class})
    public void create_PointList_ShouldThrowExceptionOnInvalidPoints() throws CreatorException {
        List<Point> tmp = new ArrayList<>();
        tmp.add(new Point(1.0, 1.0));
        tmp.add(new Point(3.0, 1.0));
        tmp.add(new Point(5.0, 1.0));
        creator.create(tmp);
    }
}
