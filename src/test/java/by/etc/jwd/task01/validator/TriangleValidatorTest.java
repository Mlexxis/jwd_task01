package by.etc.jwd.task01.validator;

import by.etc.jwd.task01.entity.Point;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class TriangleValidatorTest {

    private Validator validator = new TriangleValidator();

    @Test (groups = {"validator"})
    public void isInValidFormat_ShouldReturnTrueOnValidString() {
        boolean actual = validator.isInValidFormat("-1.0 1.0 3.5 7.0 5.0 1.0");
        Assert.assertTrue(actual);
    }

    @Test (groups = {"validator"})
    public void isInValidFormat_ShouldReturnFalseOnInvalidString() {
        boolean actual = validator.isInValidFormat("1.z0 1.0 3.5 7.0 5.0 1.0");
        Assert.assertFalse(actual);
    }

    @Test (groups = {"validator"})
    public void isInValidFormat_ShouldReturnFalseOnEmptyString() {
        boolean actual = validator.isInValidFormat("");
        Assert.assertFalse(actual);
    }

    @Test (groups = {"validator"})
    public void isInValidFormat_ShouldReturnFalseOnNull() {
        boolean actual = validator.isInValidFormat(null);
        Assert.assertFalse(actual);
    }

    @Test (groups = {"validator"})
    public  void containsValidPoints_ShouldReturnFalseOnInvalidNumberOfPoints() {
        List<Point> invalidPoints = new ArrayList<>();
        invalidPoints.add(new Point(1.0, 1.0));
        invalidPoints.add(new Point(3.0, 1.0));
        invalidPoints.add(new Point(5.0, 1.0));
        invalidPoints.add(new Point(7.0, 1.0));
        boolean actual = validator.containsValidPoints(invalidPoints);
        Assert.assertFalse(actual);
    }

    @Test (groups = {"validator"})
    public  void containsValidPoints_ShouldReturnFalseOnInvalidPoints() {
        List<Point> invalidPoints = new ArrayList<>();
        invalidPoints.add(new Point(1.0, 1.0));
        invalidPoints.add(new Point(3.0, 1.0));
        invalidPoints.add(new Point(5.0, 1.0));
        boolean actual = validator.containsValidPoints(invalidPoints);
        Assert.assertFalse(actual);
    }

    @Test (groups = {"validator"})
    public  void containsValidPoints_ShouldReturnTrueOnValidPoints() {
        List<Point> invalidPoints = new ArrayList<>();
        invalidPoints.add(new Point(1.0, 1.0));
        invalidPoints.add(new Point(3.0, 2.0));
        invalidPoints.add(new Point(5.0, 1.0));
        boolean actual = validator.containsValidPoints(invalidPoints);
        Assert.assertTrue(actual);
    }

}
