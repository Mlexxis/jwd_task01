package by.etc.jwd.task01.math_calculator;

import by.etc.jwd.task01.entity.Point;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CalculatorTest {

    @Test(groups = {"math_calculator"})
    public void calcDistanceBetweenPoints2D_ShouldReturnZeroOnEqualPoints(){
        Point point = new Point(1.0, 10.0);
        double expected = 0;
        double actual = MathCalculator.calcDistanceBetweenPoints2D(point, point);
        Assert.assertEquals(actual, expected);
    }

    @Test(groups = {"math_calculator"})
    public void calcDistanceBetweenPoints2D_ShouldReturnProperValue(){
        Point p1 = new Point(6.0, 3.0);
        Point p2 = new Point(3.0, 7.0);
        double expected = 5;
        double actual = MathCalculator.calcDistanceBetweenPoints2D(p1, p2);
        Assert.assertEquals(actual, expected);
    }
}
