package by.etc.jwd.task01.math_calculator;

import by.etc.jwd.task01.creator.TriangleCreator;
import by.etc.jwd.task01.entity.Point;
import by.etc.jwd.task01.entity.Triangle;
import by.etc.jwd.task01.exception.CreatorException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class TriangleCalculatorTest {

    private static Triangle rightTriangle;
    private static Triangle acuteTriangle;
    private static Triangle obtuseTriangle;
    private static Triangle isoscelesTriangle;
    private static Triangle equilateralTriangle;

    @BeforeTest(groups = {"math_calculator"})
    public static void setUp() {
        TriangleCreator creator = new TriangleCreator();
        List<Point> rightTrianglePoints = new ArrayList<>();
        rightTrianglePoints.add(new Point(0.0, 0.0));
        rightTrianglePoints.add(new Point(4.0, 0.0));
        rightTrianglePoints.add(new Point(4.0, 3.0));
        try {
            rightTriangle  = creator.create(rightTrianglePoints);
        } catch (CreatorException e) {
            e.printStackTrace();
        }

        List<Point> acuteTrianglePoints = new ArrayList<>();
        acuteTrianglePoints.add(new Point(0.0, 0.0));
        acuteTrianglePoints.add(new Point(6.0, 0.0));
        acuteTrianglePoints.add(new Point(4.0, 3.0));
        try {
            acuteTriangle = creator.create(acuteTrianglePoints);
        } catch (CreatorException e) {
            e.printStackTrace();
        }

        List<Point> obtuseTrianglePoints = new ArrayList<>();
        obtuseTrianglePoints.add(new Point(0.0, 0.0));
        obtuseTrianglePoints.add(new Point(3.0, 0.0));
        obtuseTrianglePoints.add(new Point(4.0, 3.0));
        try {
            obtuseTriangle = creator.create(obtuseTrianglePoints);
        } catch (CreatorException e) {
            e.printStackTrace();
        }

        List<Point> isoscelesTrianglePoints = new ArrayList<>();
        isoscelesTrianglePoints.add(new Point(0.0, 0.0));
        isoscelesTrianglePoints.add(new Point(8.0, 0.0));
        isoscelesTrianglePoints.add(new Point(4.0, 3.0));
        try {
            isoscelesTriangle = creator.create(isoscelesTrianglePoints);
        } catch (CreatorException e) {
            e.printStackTrace();
        }

        List<Point> equilateralTrianglePoints = new ArrayList<>();
        equilateralTrianglePoints.add(new Point(1.0, 1.0));
        equilateralTrianglePoints.add(new Point(4.0, 3 * Math.sqrt(3) + 1));
        equilateralTrianglePoints.add(new Point(7.0, 1.0));
        try {
            equilateralTriangle = creator.create(equilateralTrianglePoints);
        } catch (CreatorException e) {
            e.printStackTrace();
        }
    }

    @Test(groups = {"math_calculator"})
    public void isValidForTriangle_ShouldReturnFalseOnSamePoints() {
        Point p1 = new Point(6.0, 3.0);
        Point p2 = new Point(3.0, 7.0);
        boolean actual = TriangleCalculator.isValidForTriangle(p1, p2, p1);
        Assert.assertFalse(actual);
    }

    @Test(groups = {"math_calculator"})
    public void isValidForTriangle_ShouldReturnFalseOnInvalidPoints() {
        Point p1 = new Point(1.0, 1.0);
        Point p2 = new Point(3.0, 1.0);
        Point p3 = new Point(5.0, 1.0);
        boolean actual = TriangleCalculator.isValidForTriangle(p1, p2, p3);
        Assert.assertFalse(actual);
    }

    @Test(groups = {"math_calculator"})
    public void isValidForTriangle_ShouldReturnTrueOnValidPoints() {
        Point p1 = new Point(1.0, 1.0);
        Point p2 = new Point(3.0, 3.0);
        Point p3 = new Point(5.0, 1.0);
        boolean actual = TriangleCalculator.isValidForTriangle(p1, p2, p3);
        Assert.assertTrue(actual);
    }

    @Test(groups = {"math_calculator"})
    public void isRightTriangle_ShouldReturnTrueOnValidTriangle() {
        Assert.assertTrue(TriangleCalculator.isRightTriangle(rightTriangle));
    }

    @Test(groups = {"math_calculator"})
    public void isRightTriangle_ShouldReturnFalseOnInvalidTriangle() {
        Assert.assertFalse(TriangleCalculator.isRightTriangle(acuteTriangle));
    }

    @Test(groups = {"math_calculator"})
    public void isAcuteTriangle_ShouldReturnTrueOnValidTriangle() {
        Assert.assertTrue(TriangleCalculator.isAcuteTriangle(acuteTriangle));
    }

    @Test(groups = {"math_calculator"})
    public void isAcuteTriangle_ShouldReturnFalseOnInvalidTriangle() {
        Assert.assertFalse(TriangleCalculator.isAcuteTriangle(rightTriangle));
    }

    @Test(groups = {"math_calculator"})
    public void isObtuseTriangle_ShouldReturnTrueOnValidTriangle() {
        Assert.assertTrue(TriangleCalculator.isObtuseTriangle(obtuseTriangle));
    }

    @Test(groups = {"math_calculator"})
    public void isObtuseTriangle_ShouldReturnFalseOnInvalidTriangle() {
        Assert.assertFalse(TriangleCalculator.isObtuseTriangle(rightTriangle));
    }

    @Test(groups = {"math_calculator"})
    public void isIsoscelesTriangle_ShouldReturnTrueOnValidTriangle() {
        Assert.assertTrue(TriangleCalculator.isIsoscelesTriangle(isoscelesTriangle));
    }

    @Test(groups = {"math_calculator"})
    public void isIsoscelesTriangle_ShouldReturnFalseOnInvalidTriangle() {
        Assert.assertFalse(TriangleCalculator.isIsoscelesTriangle(rightTriangle));
    }

    @Test(groups = {"math_calculator"})
    public void isEquilateralTriangle_ShouldReturnTrueOnValidTriangle() {
        Assert.assertTrue(TriangleCalculator.isEquilateralTriangle(equilateralTriangle));
    }

    @Test(groups = {"math_calculator"})
    public void isEquilateralTriangle_ShouldReturnFalseOnInvalidTriangle() {
        Assert.assertFalse(TriangleCalculator.isEquilateralTriangle(rightTriangle));
    }

    @Test(groups = {"math_calculator"})
    public void square_ShouldReturnProperValue() {
        double expected = 6;
        double actual = TriangleCalculator.square(rightTriangle);
        Assert.assertEquals(actual, expected);
    }

    @Test(groups = {"math_calculator"})
    public void perimeter_ShouldReturnProperValue() {
        double expected = 12;
        double actual = TriangleCalculator.perimeter(rightTriangle);
        Assert.assertEquals(actual, expected);
    }
}
