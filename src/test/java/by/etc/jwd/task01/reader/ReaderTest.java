package by.etc.jwd.task01.reader;

import by.etc.jwd.task01.exception.ReaderException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ReaderTest {

    @Test (groups = {"reader"})
    public void read_shouldReadAllLines() {
        StringReader reader = new TextFileReader();
        int expected = 8;
        int actual = 0;
        try {
            actual = reader.read("src/test/resources/ReaderTest_DataForTriangles.txt").size();
        } catch (ReaderException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(actual, expected);
    }

    @Test (groups = {"reader"})
    public void read_shouldReadWholeLine() {
        StringReader reader = new TextFileReader();
        int expected = 40;
        int actual = 0;
        try {
            actual = reader.read("src/test/resources/ReaderTest_singleLine.txt").get(0).length();
        } catch (ReaderException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(actual, expected);
    }

    @Test (expectedExceptions = {ReaderException.class})
    public void read_shouldThrowExceptionOnInvalidPath() throws ReaderException {
        StringReader reader = new TextFileReader();
        reader.read("nowhere");
    }
}
