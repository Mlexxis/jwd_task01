package by.etc.jwd.task01.parser;

import by.etc.jwd.task01.entity.Point;
import by.etc.jwd.task01.exception.ReaderException;
import by.etc.jwd.task01.reader.StringReader;
import by.etc.jwd.task01.reader.TextFileReader;
import by.etc.jwd.task01.validator.TriangleValidator;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.Collections;
import java.util.List;

public class StringToPointsTest {

    private Parseable<Point, String> parser = new StringToPointsParser(new TriangleValidator());

    @Test(groups = {"parser"})
    public void parse_ShouldReturnProperListSize() {
        StringReader reader = new TextFileReader();
        int expected = 9;
        int actual = 0;
        try {
            actual = parser.parse(reader.read("src/test/resources/ParserTest_DataForTriangles.txt")).size();
        } catch (ReaderException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(actual, expected);
    }

    @Test(groups = {"parser"})
    public void parse_ShouldReturnEmptyListOnNoData() {
        List<Point> actual = parser.parse(Collections.emptyList());
        Assert.assertTrue(actual.isEmpty());
    }

    @Test(groups = {"parser"})
    public void parse_ShouldReturnEmptyListOnNull() {
        List<Point> actual = parser.parse(null);
        Assert.assertTrue(actual.isEmpty());
    }
}
