package by.etc.jwd.task01.parser;

import by.etc.jwd.task01.entity.Point;
import by.etc.jwd.task01.validator.Validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringToPointsParser implements Parseable<Point, String> {

    private Validator validator;

    public StringToPointsParser(Validator validator) {
        this.validator = validator;
    }

    @Override
    public List<Point> parse(List<String> source) {
        if (source == null || source.isEmpty()) {
            return Collections.emptyList();
        }

        List<Point> result = new ArrayList<>();

        source.stream()
                .filter(element -> validator.isInValidFormat(element))
                .forEach(element -> {
                    String [] tmp = element.split("\\s+");
                    for (int i = 0; i < tmp.length; i+=2) {
                        result.add(new Point(Double.parseDouble(tmp[i]), Double.parseDouble(tmp[i + 1])));
                    }
                });
        return result;
    }
}
