package by.etc.jwd.task01.parser;

import java.util.List;

public interface Parseable<E,T> {
    List<E> parse(List<T> source);
}
