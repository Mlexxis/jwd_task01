package by.etc.jwd.task01.exception;

public class CreatorException extends Exception{

    public CreatorException() {
    }

    public CreatorException(String message) {
        super(message);
    }

    public CreatorException(String message, Exception ex) {
        super(message, ex);
    }

    public CreatorException(Exception ex) {
        super(ex);
    }
}
