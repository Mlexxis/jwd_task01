package by.etc.jwd.task01.exception;

public class ReaderException extends Exception {

    public ReaderException() {
    }

    public ReaderException(String message) {
        super(message);
    }

    public ReaderException(Exception ex) {
        super(ex);
    }

    public ReaderException(String message, Exception ex) {
        super(message, ex);
    }
}
