package by.etc.jwd.task01.math_calculator;

import by.etc.jwd.task01.entity.Point;

public class MathCalculator {

    public static double calcDistanceBetweenPoints2D (Point p1, Point p2) {
        if (p1.equals(p2)) {
            return 0;
        }
        return Math.sqrt(Math.pow((p2.getX() - p1.getX()), 2) + Math.pow(p2.getY() - p1.getY(), 2));
    }
}
