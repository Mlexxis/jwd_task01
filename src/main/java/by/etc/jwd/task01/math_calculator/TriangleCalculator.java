package by.etc.jwd.task01.math_calculator;

import by.etc.jwd.task01.entity.Point;
import by.etc.jwd.task01.entity.Triangle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TriangleCalculator extends MathCalculator {

    public static boolean isValidForTriangle(Point p1, Point p2, Point p3) {
        if (p1.equals(p2) || p2.equals(p3) || p1.equals(p3)) {
            return false;
        }
        double firstEdge = MathCalculator.calcDistanceBetweenPoints2D(p1, p2);
        double secondEdge = MathCalculator.calcDistanceBetweenPoints2D(p2, p3);
        double thirdEdge = MathCalculator.calcDistanceBetweenPoints2D(p1, p3);

        return ((firstEdge + secondEdge) > thirdEdge) && ((secondEdge + thirdEdge) > firstEdge) && ((firstEdge + thirdEdge) > secondEdge);
    }

    public static boolean isRightTriangle(Triangle triangle) {
        List<Double> sortedEdges = sortedEdges(triangle);
        boolean result = Math.pow(sortedEdges.get(2), 2) == (
                Math.pow(sortedEdges.get(0), 2) + Math.pow(sortedEdges.get(1), 2));
        return result;
    }

    public static boolean isAcuteTriangle(Triangle triangle) {
        List<Double> sortedEdges = sortedEdges(triangle);
        boolean result = Math.pow(sortedEdges.get(2), 2) < (
                Math.pow(sortedEdges.get(0), 2) + Math.pow(sortedEdges.get(1), 2));
        return result;
    }

    public static boolean isObtuseTriangle(Triangle triangle) {
        List<Double> sortedEdges = sortedEdges(triangle);
        boolean result = Math.pow(sortedEdges.get(2), 2) > (
                Math.pow(sortedEdges.get(0), 2) + Math.pow(sortedEdges.get(1), 2));
        return result;
    }

    public static boolean isIsoscelesTriangle(Triangle triangle) {
        List<Double> sortedEdges = sortedEdges(triangle);
        boolean result = (sortedEdges.get(0).equals(sortedEdges.get(1)))
                            || (sortedEdges.get(0).equals(sortedEdges.get(2)))
                            || (sortedEdges.get(1).equals(sortedEdges.get(2)));
        return result;
    }

    public static boolean isEquilateralTriangle(Triangle triangle) {
        List<Double> sortedEdges = sortedEdges(triangle);
        boolean result = (sortedEdges.get(0).equals(sortedEdges.get(1)))
                && (sortedEdges.get(1).equals(sortedEdges.get(2)));
        return result;
    }

    public static double square(Triangle triangle) {
        List<Double> sortedEdges = sortedEdges(triangle);
        double halfPerimeter = perimeter(triangle) / 2;
        double result = Math.sqrt(halfPerimeter * (halfPerimeter - sortedEdges.get(0))
                                                * (halfPerimeter - sortedEdges.get(1))
                                                * (halfPerimeter - sortedEdges.get(2)));
        return result;
    }

    public static double perimeter(Triangle triangle) {
        List<Double> sortedEdges = sortedEdges(triangle);
        double perimeter = 0;
        for (Double edge : sortedEdges) {
            perimeter += edge;
        }
        return perimeter;
    }

    private static List<Double> sortedEdges(Triangle triangle) {
        List<Double> edges = new ArrayList<>();
        edges.add(MathCalculator.calcDistanceBetweenPoints2D(
                triangle.getLeftVertex(), triangle.getMiddleVertex()));
        edges.add(MathCalculator.calcDistanceBetweenPoints2D(
                triangle.getMiddleVertex(), triangle.getRightVertex()));
        edges.add(MathCalculator.calcDistanceBetweenPoints2D(
                triangle.getLeftVertex(), triangle.getRightVertex()));
        Collections.sort(edges);
        return edges;
    }

}