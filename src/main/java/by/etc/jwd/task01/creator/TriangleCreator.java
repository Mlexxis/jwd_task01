package by.etc.jwd.task01.creator;

import by.etc.jwd.task01.entity.Point;
import by.etc.jwd.task01.entity.Triangle;
import by.etc.jwd.task01.exception.CreatorException;
import by.etc.jwd.task01.validator.TriangleValidator;
import by.etc.jwd.task01.validator.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class TriangleCreator implements FactoryCreator<Triangle> {

    private static Validator validator = new TriangleValidator();
    private final static Logger log = LogManager.getRootLogger();

    @Override
    public Triangle create(List<Point> points) throws CreatorException {
        if (points == null || points.size() != 3) {
            log.error("no or wrong number of points for triangle", new CreatorException());
            throw new CreatorException("no or wrong number of points");
        }

        if (validator.containsValidPoints(points)) {
            Triangle triangle = new Triangle();
            triangle.setLeftVertex(points.get(0));
            triangle.setMiddleVertex(points.get(1));
            triangle.setRightVertex(points.get(2));
            return triangle;
        } else {
            log.error("Invalid points for triangle", new CreatorException());
            throw new CreatorException("Invalid points for triangle");
        }
    }

    @Override
    public Triangle create(double... coordinates) throws CreatorException {
        if (coordinates.length != 6) {
            log.error("no or wrong number of points for triangle", new CreatorException());
            throw new CreatorException("wrong number of coordinates");
        }

        List<Point> points = new ArrayList<>();
        for (int i = 0; i < coordinates.length; i+=2) {
            points.add(new Point(coordinates[i], coordinates[i + 1]));
        }
        return create(points);
    }
}
