package by.etc.jwd.task01.creator;

import by.etc.jwd.task01.entity.Point;
import by.etc.jwd.task01.exception.CreatorException;

import java.util.List;

public interface FactoryCreator<T> {

    T create(List<Point> points) throws CreatorException;
    T create(double ... coordinates) throws CreatorException;

}
