package by.etc.jwd.task01.validator;

import by.etc.jwd.task01.entity.Point;

import java.util.List;

public interface Validator {
    boolean isInValidFormat(String pointsString);
    boolean containsValidPoints(List<Point> points);
}
