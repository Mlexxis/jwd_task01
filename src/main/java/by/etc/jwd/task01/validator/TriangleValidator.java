package by.etc.jwd.task01.validator;

import by.etc.jwd.task01.entity.Point;
import by.etc.jwd.task01.math_calculator.TriangleCalculator;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TriangleValidator implements Validator {

    private final String POINTS_PATTERN = "(-?\\d*\\.\\d*\\s*){6}";

    @Override
    public boolean isInValidFormat(String pointsString) {
        if (pointsString == null || pointsString.length() == 0) {
            return false;
        }
        Pattern pattern = Pattern.compile(POINTS_PATTERN);
        Matcher matcher = pattern.matcher(pointsString);
        return matcher.matches();
    }

    @Override
    public boolean containsValidPoints(List<Point> points) {
        if (points.size() != 3) {
            return false;
        }
        return TriangleCalculator.isValidForTriangle(points.get(0), points.get(1), points.get(2));
    }
}
