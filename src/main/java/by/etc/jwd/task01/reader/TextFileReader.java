package by.etc.jwd.task01.reader;

import by.etc.jwd.task01.exception.ReaderException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class TextFileReader implements StringReader {

    private final static Logger log = LogManager.getRootLogger();

    @Override
    public List<String> read(String uri) throws ReaderException {
        List<String> result;
        try {
            result = Files.lines(Paths.get(uri)).collect(Collectors.toList());
        } catch (IOException ex) {
            log.error("no such file", ex);
            throw new ReaderException("no such file", ex);
        }
        return result;
    }
}
