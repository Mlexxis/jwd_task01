package by.etc.jwd.task01.reader;

import by.etc.jwd.task01.exception.ReaderException;

import java.util.List;

public interface StringReader {
    List<String> read(String uri) throws ReaderException;
}
