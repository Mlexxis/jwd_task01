package by.etc.jwd.task01.entity;

import java.io.Serializable;

public class Point implements Serializable {

    private static final long serialVersionUID = 7983443498869596561L;
    private double x;
    private double y;

    public Point() {}

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (this.getClass() != obj.getClass()) return false;
        Point anotherPoint = (Point) obj;
        return Double.compare(this.x, anotherPoint.x) == 0 && Double.compare(this.y, anotherPoint.y) == 0;
    }

    @Override
    public int hashCode() {
        return (int) (3618 * x * y) >> 4;
    }

    @Override
    public String toString() {
        return new StringBuilder(getClass().getSimpleName()).append(": {x=").append(x).append(", y=").append(y).append("}").toString();
    }
}
