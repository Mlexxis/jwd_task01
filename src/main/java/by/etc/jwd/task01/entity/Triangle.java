package by.etc.jwd.task01.entity;

import java.io.Serializable;

public class Triangle implements Serializable {

    private static final long serialVersionUID = 951059140363750680L;
    private Point leftVertex;
    private Point middleVertex;
    private Point rightVertex;

    public Triangle() {
    }

    public Point getLeftVertex() {
        return leftVertex;
    }

    public void setLeftVertex(Point leftVertex) {
        this.leftVertex = leftVertex;
    }

    public Point getMiddleVertex() {
        return middleVertex;
    }

    public void setMiddleVertex(Point middleVertex) {
        this.middleVertex = middleVertex;
    }

    public Point getRightVertex() {
        return rightVertex;
    }

    public void setRightVertex(Point rightVertex) {
        this.rightVertex = rightVertex;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || this.getClass() != obj.getClass()) return false;
        Triangle anotherTriangle = (Triangle) obj;
        return this.leftVertex.equals(anotherTriangle.leftVertex)
                && this.middleVertex.equals(anotherTriangle.middleVertex)
                && this.rightVertex.equals(anotherTriangle.rightVertex);
    }

    @Override
    public int hashCode() {
        int result = 0;
        result += (leftVertex.hashCode() + middleVertex.hashCode() + rightVertex.hashCode()) * 361;
        result >>= 2;
        return result;
    }

    @Override
    public String toString() {
        return new StringBuilder(getClass().getSimpleName())
                        .append(": {leftVertex=").append(leftVertex.getX()).append(", ").append(leftVertex.getY()).append(";")
                        .append(" middleVertex=").append(middleVertex.getX()).append(", ").append(middleVertex.getY()).append(";")
                        .append(" rightVertex=").append(rightVertex.getX()).append(", ").append(rightVertex.getY()).append("}")
                        .toString();
    }
}
